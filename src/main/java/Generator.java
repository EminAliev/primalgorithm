import java.util.Random;

public class Generator {
    public int current_edge_weight;
    private int[][] M;
    private int n;
    private int nVerts;
    private int x;
    private int y;
    private int[] next;
    private static int MAX_COST = 50;

    public static int[][] generateGraph(int a, int prob, long theseed) {

        double b;
        int i, j;

        int n = a;
        int x = 0;
        int y = 0;
        int[][] M = new int[n][n];
        int nVerts = n;
        int[] next = new int[n];

        for (i = 0; i < nVerts; i++)
            next[i] = -1;

        Random generator = new Random();
        Random generator2 = new Random(theseed);
        Random mygenerator;

        if (theseed == -1)
            mygenerator = generator;
        else
            mygenerator = generator2;
        Random random = new Random();
        for (i = 0; i < nVerts; i++) {
            for (j = 0; j < nVerts; j++) {

                if (i == j)
                    M[i][j] = 0;
                else if (j < i)
                    M[i][j] = M[j][i];
                else {
                    b = mygenerator.nextDouble() * 100;
                    if (b <= prob)
                        M[i][j] = random.nextInt(MAX_COST
                        );
                    else
                        M[i][j] = 0;
                }
            }
        }
        return M;
    }

    public void insertVertex(int a, int x, int y) {
        if (x == y) {
            if (a != 0) {
                System.out.println("Cannot initialize graph, M[i][i] must be zero!  Exiting...");
                System.exit(0);
            }
        }

        M[x][y] = a;

    }

    public static void display(int[][] M) {
        int n = M.length;
        System.out.println("");
        for (int row = 0; row < n; row++) {
            for (int col = 0; col < n; col++)
                System.out.printf(M[row][col] + "\t");
            System.out.println("");
        }
    }


    public int vertices() {
        return nVerts;

    }


    public int edgeLength(int a, int b) {
        return M[a][b];

    }


    public int nextneighbor(int v) {

        next[v] = next[v] + 1;

        if (next[v] < nVerts) {
            while (M[v][next[v]] == 0 && next[v] < nVerts) {
                next[v] = next[v] + 1;

                if (next[v] == nVerts)
                    break;
            }

        }

        if (next[v] >= nVerts) {
            next[v] = -1;
            current_edge_weight = -1;
        } else current_edge_weight = M[v][next[v]];

        return next[v];

    }

    public void resetnext() {
        for (int i = 0; i < nVerts; i++)
            next[i] = -1;

    }


    public int[][] getGraph() {
        return M;
    }

}
