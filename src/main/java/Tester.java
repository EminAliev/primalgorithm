import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Tester {

    private static int STEP = 5;
    private static int START_STEP = 100;
    private static int AMOUNT_OF_TESTS = 450;
    private static int iteration = 0;


    public static void main(String[] args) {
        String path = "";
        int[][] graph;

        try {
            File test = new File(path + "times " + ".csv");
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(test));
            int vertex;
            File test1 = new File(path + "iter" + ".csv");
            BufferedWriter bufferedWriter1 = new BufferedWriter(new FileWriter(test1));

            for (int i = 0; i < AMOUNT_OF_TESTS; i++) {
                vertex = START_STEP + i * STEP;
                graph = Generator.generateGraph(vertex, vertex * 2, -1);
                bufferedWriter.write(vertex + "," + runTest(graph) + "\n");
                bufferedWriter1.write(vertex + "," + iteration + "\n");
                System.out.println("Test #" + i);

            }
            bufferedWriter.close();
            bufferedWriter1.close();


        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public static long runTest(int[][] graph) {
        long before = System.nanoTime();
        PrimsAlgoritm.primFindMST(graph);
        iteration = PrimsAlgoritm.getIter();
        long after = System.nanoTime();
        System.gc();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //TODO : 5 times test algorithm
        return after - before;
    }
}
